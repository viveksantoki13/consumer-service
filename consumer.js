const amqp = require("amqplib");

 async function connect() {
      try{
        const connection  = await amqp.connect("amqp://localhost:5672");
        const channel = await connection.createChannel();
        await channel.assertQueue("dataTransfer");
        channel.consume("dataTransfer", (msg) => {
          const input = JSON.parse(msg.content.toString());
          channel.ack(msg);
        });
      }
      catch(err){
        console.log(err);
      }
 };

 connect();