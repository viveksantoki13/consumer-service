const Hapi = require("hapi");
const apiRoot = require('./server/api');
const es = require('elasticsearch');
import { consumer } from './server/mqAggent';

const server = new Hapi.Server({ "host": "localhost", "port": 3000 })

const esClient = new es.Client({
    host: 'localhost:9200',
});

server.start();

 function init(server,esClient) {
  consumer(esClient);
  apiRoot(server,esClient);
  const checkNCreateIndex = require('./server/utils')(esClient);
  checkNCreateIndex();
}

init(server,esClient);
