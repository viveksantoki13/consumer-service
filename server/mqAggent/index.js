const amqp = require("amqplib");

export async function consumer(esClient) {
  const gameType = require('../api/game/gameTypeHandler')(esClient);
  try{
        const connection  = await amqp.connect("amqp://localhost:5672");
        const channel = await connection.createChannel();
        await channel.assertQueue("dataTransfer");
        channel.consume("dataTransfer", (msg) => {
        const input = JSON.parse(msg.content.toString());
        const request= {
          payload: input,
        }
        gameType.addGameData(request);
        channel.ack(msg);
        });
      }
      catch(err){
        console.log(err);
      }
  };
