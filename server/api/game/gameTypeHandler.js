module.exports = (esClient) => {

  const gameType = {}

  const calculateWinAmount = (response) => {
    const winAmount = response.reduce((acc, current)=>{
      if(!current._source.winAmount){
        return acc - current._source.betAmount;
      }
      return acc + current._source.winAmount;
    },0);
    return winAmount;
  }

  gameType.getBalanceByuserId = async (request) => {
    try{
      let response = await esClient.search({
        index: 'provider',
        type: 'provider',
        body: {
          "_source": ["betAmount", "winAmount"],
          query: {
              match: request.query
          }
        }
      });
      response = calculateWinAmount(response.hits.hits);
      return {
        statusCode: 200,
        data: {
          winAmount: response,
        },
      }
    }
    catch(err){
      console.log(err);
    }
  }


  gameType.getBalanceBygameId = async (request) => {
    try{
      let response = await esClient.search({
        index: 'provider',
        type: 'provider',
        body: {
          "_source": ["betAmount", "winAmount"],
          query: {
              match: request.query
          }
        }
      });
      response = calculateWinAmount(response.hits.hits);
      return {
        statusCode: 200,
        data: {
          winAmount: response,
        },
      }
    }
    catch(err){
      console.log(err);
    }
  }

  gameType.addGameData = async (request) => {
    try{
      const response = await esClient.index({
        index: 'provider',
        type: 'provider',
        body: request.payload
    });
    if(response){
      return {
        statusCode: 200,
        data: {
          message: 'Data inserted successfully',
        },
      };
    }
    }
    catch(err){
      console.log(err);
    }
  }
  return gameType;
};
