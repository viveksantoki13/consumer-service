
const Joi = require('joi');

module.exports = (server,esClient) => {
  const gameType = require('./gameTypeHandler')(esClient);

  server.route({
    method: 'GET',
    path: '/api/v1/game/userId',
    handler: gameType.getBalanceByuserId,
    config: {
      validate: {
        query: {
          userId: Joi.string().required(),
        },
      },
    },
  });

  server.route({
    method: 'GET',
    path: '/api/v1/game/gameId',
    handler: gameType.getBalanceBygameId,
    config: {
      validate: {
        query: {
          gameId: Joi.string().required(),
        },
      },
    },
  });

  server.route({
    method: 'POST',
    path: '/api/v1/game',
    handler: gameType.addGameData,
    config: {
      validate: {
        payload: {
          userId: Joi.string().required(),
          gameId: Joi.string().required(),
          transactionId: Joi.string().required(),
          betAmount: Joi.number().required(),
          winAmount: Joi.number().required(),
        }
      }
    }
  })
}