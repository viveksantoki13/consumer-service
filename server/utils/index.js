const providerIndex = async esClient => {
  try{
    await isProviderIndexExist(esClient);
  }
  catch(err){
    console.log(err);
  }
};

module.exports = esClient => {
  return function checkNCreateIndex() {
    providerIndex(esClient);
  };
};

function isProviderIndexExist(esClient) {
  return esClient.indices.exists({index: 'provider'}, (err, res, status) => {
       if (res) {
           return res;
       } else {
           esClient.indices.create( {index: 'provider'}, (err, res, status) => {
           if(res){
             const providerIndexMapping = require('../mappings/provider_mappings');
             esClient.indices.putMapping({
               index: 'provider',
               type: 'provider',
               body:  providerIndexMapping,
           }, (err,resp, status) => {
               if (err) {
                 console.error(err, status);
               }
               else {
                   console.log('Successfully Created Index', status, resp);
               }
           });
           }
       })
     }
   })
 };