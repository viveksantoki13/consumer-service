function isProviderIndexExist(esClient) {
 return esClient.indices.exists({index: 'provider'}, (err, res, status) => {
      if (res) {
          console.log('index already exists');
          return res;
      } else {
          esClient.indices.create( {index: 'provider'}, (err, res, status) => {
          if(res){
            const providerIndexMapping = require('../mappings/provider_mappings');
            esClient.indices.putMapping({
              index: 'provider',
              type: 'provider',
              body:  providerIndexMapping,
          }, (err,resp, status) => {
              if (err) {
                console.error(err, status);
              }
              else {
                  console.log('Successfully Created Index', status, resp);
              }
          });
          }
      })
    }
  })
};

module.exports = {
  isProviderIndexExist,
}